curl https://upload.bugsnag.com/ \
  -F apiKey=51f60d5e29484e49a40f4a862428c35a \
  -F appVersion=1.0 \
  -F minifiedUrl="http://localhost:8081/index.ios.bundle?platform=ios&dev=true&minify=false" \
  -F sourceMap=@./tools/index.ios.json \
  -F minifiedFile=@./tools/index.ios.bundle \
  -F overwrite=true
